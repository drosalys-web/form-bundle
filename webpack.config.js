const path = require('path');

const config = {
    mode: 'development',
    entry: {
        'form-collection': path.resolve(__dirname, 'assets/scripts/index.ts'),
    },
    output: {
        path: path.resolve(__dirname, 'bundles'),
        filename: '[name].js',
        libraryTarget: 'umd',
        library: 'DrosalysFormCollection',
        umdNamedDefine: true,
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules|src|vendor/,
            },
        ],
    },
};

module.exports = (env, argv) => {
    if (argv.mode === 'production') {
        config.mode = 'production';
        Object.assign(config.output, {
            filename: '[name].min.js',
        });
    }

    return config;
};
