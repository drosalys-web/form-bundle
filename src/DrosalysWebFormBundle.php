<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle;

use DrosalysWeb\Bundle\FormBundle\DependencyInjection\Compiler\CollectionTemplatePass;
use DrosalysWeb\Bundle\FormBundle\DependencyInjection\Compiler\SelectOrCreateTemplatePass;
use DrosalysWeb\Bundle\FormBundle\DependencyInjection\DrosalysWebFormExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DrosalysWebFormBundle
 *
 * @author Benjamin Georgeault
 */
class DrosalysWebFormBundle extends Bundle
{
    /**
     * @inheritDoc
     */
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new DrosalysWebFormExtension();
    }

    /**
     * @inheritDoc
     */
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new CollectionTemplatePass());
        $container->addCompilerPass(new SelectOrCreateTemplatePass());
    }
}
