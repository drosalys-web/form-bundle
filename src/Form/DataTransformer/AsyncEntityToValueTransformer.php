<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\Form\DataTransformer;

use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\Mapping\ClassMetadata;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class AsyncEntityToValueTransformer
 *
 * @author Benjamin Georgeault
 */
class AsyncEntityToValueTransformer implements DataTransformerInterface
{
    private EntityRepository $repo;
    private ClassMetadata $metadata;

    public function __construct(EntityRepository $repo, ClassMetadata $metadata)
    {
        $this->repo = $repo;
        $this->metadata = $metadata;
    }

    public function transform($entity)
    {
        if (empty($entity)) {
            return null;
        }

        return array_values($this->metadata->getIdentifierValues($entity))[0];
    }

    public function reverseTransform($id)
    {
        if (empty($id)) {
            return null;
        }

        if (null === $entity = $this->repo->find($id)) {
            throw new TransformationFailedException(sprintf(
                'The entity "%s" with id "%s" does not exist.',
                $this->repo->getClassName(),
                $id,
            ));
        }

        return $entity;
    }
}
