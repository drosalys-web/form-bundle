<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\Form\DataTransformer;

use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\Mapping\ClassMetadata;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class AsyncEntitiesToValuesTransformer
 *
 * @author Benjamin Georgeault
 */
class AsyncEntitiesToValuesTransformer implements DataTransformerInterface
{
    private EntityRepository $repo;
    private ClassMetadata $metadata;

    public function __construct(EntityRepository $repo, ClassMetadata $metadata)
    {
        $this->repo = $repo;
        $this->metadata = $metadata;
    }

    public function transform($entities)
    {
        if (null === $entities) {
            return [];
        }

        if (!\is_array($entities)) {
            throw new TransformationFailedException('Expected an array.');
        }

        return array_map(function ($entity) {
            return array_values($this->metadata->getIdentifierValues($entity))[0];
        }, $entities);
    }

    public function reverseTransform($ids)
    {
        if (null === $ids) {
            return [];
        }

        if (!\is_array($ids)) {
            throw new TransformationFailedException('Expected an array.');
        }

        $idField = $this->metadata->getIdentifierFieldNames()[0];
        $entities = $this->repo->findBy([
            $idField => $ids,
        ]);

        if (\count($entities) !== \count($ids)) {
            throw new TransformationFailedException('Could not find all matching entities for the given ids.');
        }

        return $entities;
    }
}
