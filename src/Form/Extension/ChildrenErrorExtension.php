<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Class ChildrenErrorExtension
 *
 * @author Benjamin Georgeault
 */
class ChildrenErrorExtension extends AbstractTypeExtension
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (false === $containErrors = 0 < $form->getErrors()->count()) {
            /** @var FormInterface $child */
            foreach ($form as $child) {
                if (0 < $child->getErrors()->count()) {
                    $containErrors = true;
                    break;
                }
            }
        }

        $view->vars['contain_errors'] = $containErrors;
    }

    public static function getExtendedTypes(): iterable
    {
        yield FormType::class;
    }
}
