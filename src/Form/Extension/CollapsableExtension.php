<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CollapsableExtension
 *
 * @author Benjamin Georgeault
 */
class CollapsableExtension extends AbstractTypeExtension
{
    /**
     * @inheritDoc
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['collapsable']) {
            $initialState = json_encode($options['collapsable_initial_state']);

            $view->vars['label_attr'] = array_merge($view->vars['label_attr'], [
                'data-form-collapser' => '#'.$view->vars['id'],
                'data-form-collapser-state' => $initialState,
            ]);

            $view->vars['attr'] = array_merge($view->vars['attr'], [
                'data-form-collapsable' => $initialState,
            ]);

            $view->vars['row_attr'] = array_merge($view->vars['row_attr'], [
                'data-form-collapse' => '#'.$view->vars['id'],
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'collapsable' => false,
            'collapsable_initial_state' => true,
        ]);

        $resolver->addAllowedTypes('collapsable', 'boolean');
        $resolver->addAllowedTypes('collapsable_initial_state', 'boolean');
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [
            FormType::class,
        ];
    }
}
