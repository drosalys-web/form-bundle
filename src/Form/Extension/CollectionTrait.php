<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\Form\Extension;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\ResolvedFormTypeInterface;

/**
 * Trait CollectionTrait
 *
 * @author Benjamin Georgeault
 */
trait CollectionTrait
{
    /**
     * @param ResolvedFormTypeInterface $resolvedFormType
     *
     * @return bool
     */
    private function isCollection(ResolvedFormTypeInterface $resolvedFormType): bool
    {
        $innerType = $resolvedFormType->getInnerType();

        if ($innerType instanceof CollectionType) {
            return true;
        }

        if (null !== $parent = $resolvedFormType->getParent()) {
            return $this->isCollection($parent);
        }

        return false;
    }

    /**
     * @param FormInterface $form
     *
     * @return bool
     */
    private function isInCollection(FormInterface $form): bool
    {
        if (null !== $parent = $form->getParent()) {
            return $this->isCollection($parent->getConfig()->getType());
        }

        return false;
    }

    /**
     * @param FormInterface $form
     * @return bool
     */
    private function isPrototype(FormInterface $form): bool
    {
        return !in_array($form, $form->getParent()->all());
    }
}
