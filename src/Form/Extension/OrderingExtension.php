<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\Form\Extension;

use DrosalysWeb\Bundle\FormBundle\Exception\NotACollectionChildException;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderingExtension
 *
 * @author Benjamin Georgeault
 */
class OrderingExtension extends AbstractTypeExtension
{
    use CollectionTrait;

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['ordering_inner']) {
            $view->vars['ordering_inner'] = true;
            $view->vars['ordering_inner_field'] = $view->children[$options['ordering_inner_field']];
            $view->vars['row_attr']['data-prototype-inner-ordering'] = '#'.$view->parent->vars['id'];
            $view->vars['label_attr']['data-prototype-inner-ordering-trigger'] = '#'.$view->vars['id'].'__collection_item';
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['ordering_inner'] && !$this->isInCollection($form)) {
            throw new NotACollectionChildException(sprintf(
                'Cannot apply ordering. Form with ID "%s" is not a direct child of a "%s".',
                $view->vars['id'],
                CollectionType::class
            ));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['ordering_inner']) {
            $builder->add($options['ordering_inner_field'], HiddenType::class, [
                'mapped' => $options['ordering_inner_field_mapped'],
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'ordering_inner' => false,
            'ordering_inner_field_mapped' => true,
            'ordering_inner_field' => 'ordering',
        ]);

        $resolver->addAllowedTypes('ordering_inner', 'boolean');
        $resolver->addAllowedTypes('ordering_inner_field_mapped', 'boolean');
        $resolver->addAllowedTypes('ordering_inner_field', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [
            FormType::class,
        ];
    }
}
