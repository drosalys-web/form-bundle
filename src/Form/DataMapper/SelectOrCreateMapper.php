<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\Form\DataMapper;

use Doctrine\Persistence\Mapping\ClassMetadata;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\FormInterface;

/**
 * Class SelectOrCreateMapper
 *
 * @author Benjamin Georgeault
 */
class SelectOrCreateMapper implements DataMapperInterface
{
    private ClassMetadata $metadata;

    public function __construct(ClassMetadata $metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @param object|null $viewData
     */
    public function mapDataToForms($viewData, \Traversable $forms): void
    {
        if (null === $viewData) {
            return;
        }

        if (!is_object($viewData)) {
            throw new UnexpectedTypeException($viewData, 'object');
        }

        /** @var FormInterface[] $forms */
        $forms = iterator_to_array($forms);

        if (empty($this->metadata->getIdentifierValues($viewData))) {
            $forms['create']->setData($viewData);
            $forms['state']->setData('create');
        } else {
            $forms['select']->setData($viewData);
            $forms['state']->setData('select');
        }
    }

    public function mapFormsToData(\Traversable $forms, &$viewData): void
    {
        /** @var FormInterface[] $forms */
        $forms = iterator_to_array($forms);

        if ('select' === $forms['state']->getData()) {
            $viewData = $forms['select']->getData();
        } else {
            $viewData = $forms['create']->getData();
        }
    }
}
