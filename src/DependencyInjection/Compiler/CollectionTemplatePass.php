<?php
/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class CollectionTemplatePass
 *
 * @author Benjamin Georgeault
 */
class CollectionTemplatePass implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasParameter('drosalys_form.collection.import_template')) {
            return;
        }

        if (true === $container->getParameter('drosalys_form.collection.import_template')) {
            $resources = $container->getParameter('twig.form.resources');
            $resources[] = '@DrosalysWebForm/Form/collection_layout.html.twig';
            $container->setParameter('twig.form.resources', $resources);
        }
    }
}
