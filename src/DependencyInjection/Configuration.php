<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritdoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('drosalys_form');

        $treeBuilder->getRootNode()
            ->children()
                ->append($this->collectionNode())
                ->append($this->collapsableNode())
                ->append($this->tabsNode())
                ->append($this->orderingNode())
                ->append($this->asyncNode())
                ->append($this->selectOrCreateNode())
            ->end()
        ;

        return $treeBuilder;
    }

    /**
     * @return NodeDefinition
     */
    private function collectionNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('collection');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->booleanNode('import_template')
                    ->defaultTrue()
                ->end()
                ->scalarNode('main_form_template')
                    ->defaultValue('form_div_layout.html.twig')
                ->end()
            ->end()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function collapsableNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('collapsable');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function tabsNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('tabs');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function orderingNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('ordering');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function asyncNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('async');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->scalarNode('doctrine_connection')
                    ->cannotBeEmpty()
                    ->defaultValue('default')
                ->end()
            ->end()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function selectOrCreateNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('select_or_create');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->scalarNode('doctrine_connection')
                    ->cannotBeEmpty()
                    ->defaultValue('default')
                ->end()
                ->booleanNode('import_template')
                    ->defaultTrue()
                ->end()
                ->scalarNode('layout_template')
                    ->cannotBeEmpty()
                    ->defaultValue('@DrosalysWebForm/Form/select_or_create_layout.html.twig')
                ->end()
            ->end()
        ;

        return $node;
    }
}
