<?php

/*
 * This file is part of the form-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\FormBundle\DependencyInjection;

use Doctrine\ORM\EntityManagerInterface;
use DrosalysWeb\Bundle\FormBundle\Form\Type\AsyncEntityType;
use DrosalysWeb\Bundle\FormBundle\Form\Type\SelectOrCreateType;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DrosalysWebFormExtension
 *
 * @author Benjamin Georgeault
 */
class DrosalysWebFormExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        $childrenErrors = false;

        if ($config['collection']['enabled']) {
            $loader->load('collection.yaml');
            $container->setParameter('drosalys_form.collection.import_template', $config['collection']['import_template']);
            $container->setParameter('drosalys_form.collection.main_form_template', $config['collection']['main_form_template']);
        }

        if ($config['collapsable']['enabled']) {
            $loader->load('collapsable.yaml');
            $childrenErrors = true;
        }

        if ($config['tabs']['enabled']) {
            $loader->load('tabs.yaml');
            $childrenErrors = true;
        }

        if ($config['ordering']['enabled']) {
            $loader->load('ordering.yaml');
        }

        if ($config['async']['enabled']) {
            $loader->load('async.yaml');

            $container->setDefinition('drosalys_form.async.entity_manager', new Definition(EntityManagerInterface::class))
                ->setFactory([new Reference('doctrine'), 'getManager'])
                ->setArguments([
                    $config['async']['doctrine_connection'],
                ])
            ;

            $container->getDefinition(AsyncEntityType::class)
                ->setArgument(0, new Reference('drosalys_form.async.entity_manager'))
            ;
        }

        if ($config['select_or_create']['enabled']) {
            $container->setParameter('drosalys_form.select_or_create.import_template', $config['select_or_create']['import_template']);
            $container->setParameter('drosalys_form.select_or_create.layout_template', $config['select_or_create']['layout_template']);
            $loader->load('select_or_create.yaml');

            $container->setDefinition('drosalys_form.select_or_create.entity_manager', new Definition(EntityManagerInterface::class))
                ->setFactory([new Reference('doctrine'), 'getManager'])
                ->setArguments([
                    $config['select_or_create']['doctrine_connection'],
                ])
            ;

            $container->getDefinition(SelectOrCreateType::class)
                ->setArgument(0, new Reference('drosalys_form.select_or_create.entity_manager'))
            ;
        }

        if ($childrenErrors) {
            $loader->load('children_error.yaml');
        }
    }

    /**
     * @inheritDoc
     */
    public function getAlias(): string
    {
        return 'drosalys_form';
    }
}
